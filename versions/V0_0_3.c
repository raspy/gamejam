/*---------------------------------Description---------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_0_3
               Multijoueur

Description Jeu : C'est un casse briques avec plusieurs joueurs.

Description : Apparitions des briques + balles + barres
            Rebond des balles sur les bords de l'écran + barres + briques

Objectif :  Faire réapparaître les briques après leur destruction

Atteint :   OUI
*/
/*-----------------------------------------------------------------------------*/

#include <time.h>

#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

#define WINDOW_WIDTH 950
#define WINDOW_HEIGHT 950
#define FPS 60

#define VITESSEBALLE 2
#define VITESSEBARRE 3

int nombreLigne = 8;
int nombreColonne = 8;

typedef struct Bricks
{
    int x;
    int y;
    int vie;
    int spawn;
    int type;
    int frame;
    int rebond;
    int temps;
    char* sourceIMG;
}T_Bricks;

T_Bricks tabBrick[8][8];

typedef struct Balls
{
    int x;
    int y;
    int color_red;
    int color_green;
    int color_blue;
    int vitesseX;
    int vitesseY;
}T_Balls;

T_Balls balles[4];

typedef struct Barres
{
    int x;
    int y;
    int color_red_contour;      
    int color_green_contour;    
    int color_blue_contour;    
    int color_red_interieur;      
    int color_green_interieur;    
    int color_blue_interieur;  
    int vitesse;
    int longueur;
    int largeur; 
}T_Barres;

T_Barres barres[4];

int longueurBarre;
int largeurBarre;

int longueur;
int xdebutBrick;
int ydebutBrick;

int rayonBall;

char* scr;

int nombreJoueur;

void init_vieBricks(int i, int j){
    tabBrick[i][j].type = rand() % 101; //[0-100]
    if((tabBrick[i][j].type >= 0) && (tabBrick[i][j].type < 76))    tabBrick[i][j].vie = 1;
    else if((tabBrick[i][j].type < 91))                             tabBrick[i][j].vie = 2;
    else if((tabBrick[i][j].type < 99))                             tabBrick[i][j].vie = 3;
    else                                                            tabBrick[i][j].vie = 4;
}

void spawnBricks(int i, int j){
    if((tabBrick[i][j].frame == 0) && (tabBrick[i][j].spawn == 0)){
        if(tabBrick[i][j].vie == 1)         tabBrick[i][j].sourceIMG = "assets/brick6Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick6Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick6Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick6Type4.bmp";
    }         
      
    else if((tabBrick[i][j].frame == 2) && (tabBrick[i][j].spawn == 0)){
        if(tabBrick[i][j].vie == 1)         tabBrick[i][j].sourceIMG = "assets/brick5Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick5Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick5Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick5Type4.bmp";
    }   
      
    else if((tabBrick[i][j].frame == 4) && (tabBrick[i][j].spawn == 0)){
        if(tabBrick[i][j].vie == 1)         tabBrick[i][j].sourceIMG = "assets/brick4Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick4Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick4Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick4Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 6) && (tabBrick[i][j].spawn == 0)){
        if(tabBrick[i][j].vie == 1)         tabBrick[i][j].sourceIMG = "assets/brick3Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick3Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick3Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick3Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 8) && (tabBrick[i][j].spawn == 0)){
        if(tabBrick[i][j].vie == 1)         tabBrick[i][j].sourceIMG = "assets/brick2Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick2Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick2Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick2Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 10) && (tabBrick[i][j].spawn == 0)){
        if(tabBrick[i][j].vie == 1)         tabBrick[i][j].sourceIMG = "assets/brick1Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick1Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick1Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick1Type4.bmp";
    }    
      
    else if((tabBrick[i][j].frame == 12) && (tabBrick[i][j].spawn == 0)){
        if(tabBrick[i][j].vie == 1)         tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick0Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick0Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick0Type4.bmp";
    }     
      
    if(tabBrick[i][j].frame == 12) tabBrick[i][j].spawn = 1;
}

void breakBricks(int i, int j){
    if((tabBrick[i][j].frame == 0) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))            tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
      
    else if((tabBrick[i][j].frame == 2) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick1Type1.bmp";
      
    else if((tabBrick[i][j].frame == 4) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick2Type1.bmp";
      
    else if((tabBrick[i][j].frame == 6) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick3Type1.bmp";
      
    else if((tabBrick[i][j].frame == 8) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))       tabBrick[i][j].sourceIMG = "assets/brick4Type1.bmp";
      
    else if((tabBrick[i][j].frame == 10) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))      tabBrick[i][j].sourceIMG = "assets/brick5Type1.bmp";
      
    else if((tabBrick[i][j].frame == 12) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0))      tabBrick[i][j].sourceIMG = "assets/brick6Type1.bmp";
      

    if((tabBrick[i][j].frame == 12) && (tabBrick[i][j].rebond == 1) && (tabBrick[i][j].vie == 0)) tabBrick[i][j].rebond = 2;

}

void showBricks(int i, int j){
    if(tabBrick[i][j].spawn == 1){
        if(tabBrick[i][j].vie == 1)         tabBrick[i][j].sourceIMG = "assets/brick0Type1.bmp";
        else if(tabBrick[i][j].vie == 2)    tabBrick[i][j].sourceIMG = "assets/brick0Type2.bmp";
        else if(tabBrick[i][j].vie == 3)    tabBrick[i][j].sourceIMG = "assets/brick0Type3.bmp";
        else                                tabBrick[i][j].sourceIMG = "assets/brick0Type4.bmp";
    }
}

void rebondBalle_Bord(int i, int j){
    if((balles[i].vitesseX < 0) && (balles[i].x - rayonBall <= 0) || (balles[i].vitesseX > 0) && (balles[i].x + rayonBall >= WINDOW_WIDTH) ||
       (balles[i].vitesseX < 0) && (balles[i].y >= barres[j].y) && (balles[i].y <= barres[j].y + largeurBarre) && (balles[i].x - rayonBall <= barres[j].x + longueurBarre) && (balles[i].x > barres[j].x + longueurBarre) ||
       (balles[i].vitesseX > 0) && (balles[i].y >= barres[j].y) && (balles[i].y <= barres[j].y + largeurBarre) && (balles[i].x + rayonBall >= barres[j].x) && (balles[i].x < barres[j].x)
      ){
        balles[i].vitesseX = -balles[i].vitesseX;
    }

    else if((balles[i].vitesseY < 0) && (balles[i].y - rayonBall <= 0) || (balles[i].vitesseY > 0) && (balles[i].y + rayonBall >= WINDOW_HEIGHT) ||
            (balles[i].vitesseY > 0) && (balles[i].y - rayonBall >= 50 + largeurBarre) && (balles[i].x >= barres[j].x) && (balles[i].x <= barres[j].x + longueurBarre) && (balles[i].y + rayonBall >= barres[j].y) && (balles[i].y < barres[j].y) ||
            (balles[i].vitesseY < 0) && (balles[i].y - rayonBall <= WINDOW_HEIGHT - (50 + largeurBarre)) && (balles[i].x >= barres[j].x) && (balles[i].x <= barres[j].x + longueurBarre) && (balles[i].y - rayonBall <= barres[j].y + largeurBarre) && (balles[i].y > barres[j].y + largeurBarre)  
    
      ){
        balles[i].vitesseY = -balles[i].vitesseY;
    }

    else if((balles[i].vitesseX > 0) && (balles[i].vitesseY > 0) && (balles[i].x < barres[j].x) && (balles[i].y < barres[j].y) && (balles[i].x + rayonBall >= barres[j].x) && (balles[i].y + rayonBall >= barres[j].y) ||
            (balles[i].vitesseX < 0) && (balles[i].vitesseY > 0) && (balles[i].x > barres[j].x + longueurBarre) && (balles[i].x - rayonBall <= barres[j].x + longueurBarre) && (balles[i].y < barres[j].y) && (balles[i].y + rayonBall >= barres[j].y)
    ){
        balles[i].vitesseX = -balles[i].vitesseX;
        balles[i].vitesseY = -balles[i].vitesseY;
    }
}

void rebondBalle_Bricks(int i, int j, int k){
    if((balles[k].vitesseX < 0) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].y >= tabBrick[i][j].y) && (balles[k].y <= tabBrick[i][j].y + longueur) ||
       (balles[k].vitesseX > 0) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].x < tabBrick[i][j].x) && (balles[k].y >= tabBrick[i][j].y) && (balles[k].y <= tabBrick[i][j].y + longueur) 
    ){
        balles[k].vitesseX = -balles[k].vitesseX;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }

    else if((balles[k].vitesseY < 0) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].x >= tabBrick[i][j].x) && (balles[k].x <= tabBrick[i][j].x + longueur) ||
       (balles[k].vitesseY > 0) && (balles[k].y + rayonBall >= tabBrick[i][j].y) && (balles[k].y < tabBrick[i][j].y) && (balles[k].x >= tabBrick[i][j].x) && (balles[k].x <= tabBrick[i][j].x + longueur)
    ){
        balles[k].vitesseY = -balles[k].vitesseY;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }
    //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
    else if((tabBrick[i - 1][j].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i - 1][j].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i + 1][j].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (tabBrick[i + 1][j].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
    ){
        balles[k].vitesseX = -balles[k].vitesseX;
        tabBrick[i][j].vie--; 
        tabBrick[i][j].rebond = 1;                
    }
    //Coins : Haut Droit -- Haut Gauche -- Bas Droit -- Bas Gauche
    else if((tabBrick[i][j - 1].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i][j + 1].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (tabBrick[i][j - 1].vie > 0) && (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (tabBrick[i][j + 1].vie > 0) && (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
    ){
        balles[k].vitesseY = -balles[k].vitesseY;
        tabBrick[i][j].vie--;   
        tabBrick[i][j].rebond = 1;             
    }
    else if((balles[k].vitesseX > 0) && (balles[k].vitesseY > 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (balles[k].vitesseX < 0) && (balles[k].vitesseY > 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y < tabBrick[i][j].y) && (balles[k].y + rayonBall >= tabBrick[i][j].y) ||
            (balles[k].vitesseX > 0) && (balles[k].vitesseY < 0) && (balles[k].x < tabBrick[i][j].x) && (balles[k].x + rayonBall >= tabBrick[i][j].x) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur) ||
            (balles[k].vitesseX < 0) && (balles[k].vitesseY < 0) && (balles[k].x > tabBrick[i][j].x + longueur) && (balles[k].x - rayonBall <= tabBrick[i][j].x + longueur) && (balles[k].y > tabBrick[i][j].y + longueur) && (balles[k].y - rayonBall <= tabBrick[i][j].y + longueur)
       ){
        balles[k].vitesseX = -balles[k].vitesseX;
        balles[k].vitesseY = -balles[k].vitesseY;
        tabBrick[i][j].vie--;
        tabBrick[i][j].rebond = 1;
    }
}

void rebondBarre(int i){
    if(barres[i].x <= 0){
        barres[i].x = 1;
        barres[i].vitesse = 0;
    }
    else if(barres[i].x + longueurBarre >= WINDOW_WIDTH){
        barres[i].x = WINDOW_WIDTH - longueurBarre - 1;
        barres[i].vitesse = 0;
    }
}

void drawBalle(int i, int j){
    /*-----------------------------Balle 1---------------------------*/
    changeColor(balles[i].color_red, balles[i].color_green, balles[i].color_blue, 255);
    drawCircle(balles[i].x, balles[i].y, rayonBall);

    balles[i].x += balles[i].vitesseX;
    balles[i].y += balles[i].vitesseY;
    /*---------------------------------------------------------------*/

    rebondBalle_Bord(i, j);
}

void drawBarre(int i){
    changeColor(barres[i].color_red_contour, barres[i].color_green_contour, barres[i].color_blue_contour, 255);
    drawRect(barres[i].x, barres[i].y, longueurBarre, largeurBarre);
    changeColor(barres[i].color_red_interieur, barres[i].color_green_interieur, barres[i].color_blue_interieur, 255);
    drawRect(barres[i].x + 2, barres[i].y + 2, longueurBarre - 4, largeurBarre - 4);

    barres[i].x += barres[i].vitesse;

    rebondBarre(i);
}

void drawBricks(int i, int j, int k){
    /*----------------------------Briques----------------------------*/
    if(tabBrick[i][j].vie > 0){
        spawnBricks(i, j);
        showBricks(i, j);
        sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);

        rebondBalle_Bricks(i, j, k);
    }
    else if(tabBrick[i][j].rebond == 1){
        breakBricks(i, j);
        sprite(tabBrick[i][j].x, tabBrick[i][j].y, tabBrick[i][j].sourceIMG);
    }
    else{
        tabBrick[i][j].temps++;
        if(tabBrick[i][j].temps == 10 * FPS){
            tabBrick[i][j].spawn = 0;
            tabBrick[i][j].rebond = 0;
            tabBrick[i][j].frame = 0;

            init_vieBricks(i, j);
        }
    }

    if(tabBrick[i][j].frame == 12){
        tabBrick[i][j].frame = 0;
    }else{
        tabBrick[i][j].frame++;
    }
    /*---------------------------------------------------------------*/
}

void init_game(){
    /*-----------------------------Balle-----------------------------*/
    balles[0].x = WINDOW_WIDTH / 2;
    balles[0].y = WINDOW_HEIGHT - 50 - 25;
    balles[0].color_red = 244;
    balles[0].color_green = 37;
    balles[0].color_blue = 17;
    balles[0].vitesseX = VITESSEBALLE;
    balles[0].vitesseY = -VITESSEBALLE;


    balles[1].x = WINDOW_WIDTH / 2;
    balles[1].y = 50 + 25;
    balles[1].color_red = 244;
    balles[1].color_green = 214;
    balles[1].color_blue = 17;
    balles[1].vitesseX = VITESSEBALLE;
    balles[1].vitesseY = VITESSEBALLE;

    rayonBall = 10;
    /*---------------------------------------------------------------*/

    /*-----------------------------Barre-----------------------------*/
    longueurBarre = 100;
    largeurBarre = 10;

    barres[0].x = (WINDOW_WIDTH - longueurBarre) / 2;
    barres[0].y = WINDOW_HEIGHT - 50;
    barres[0].color_red_contour = 244;
    barres[0].color_green_contour = 37;
    barres[0].color_blue_contour = 17;
    barres[0].color_red_interieur = 91;
    barres[0].color_green_interieur = 19;
    barres[0].color_blue_interieur = 12;

    barres[1].x = (WINDOW_WIDTH - longueurBarre) / 2;
    barres[1].y = 50;
    barres[1].color_red_contour = 244;
    barres[1].color_green_contour = 214;
    barres[1].color_blue_contour = 17;
    barres[1].color_red_interieur = 63;
    barres[1].color_green_interieur = 91;
    barres[1].color_blue_interieur = 12;
    /*---------------------------------------------------------------*/

    /*----------------------------Briques----------------------------*/
    srand(time(NULL));

    longueur = 60;
    xdebutBrick = (WINDOW_WIDTH - (nombreColonne*longueur + (nombreColonne-1)*5))/2; //5: espace entre briques
    ydebutBrick = (WINDOW_HEIGHT - (nombreLigne*longueur + (nombreLigne-1)*5))/2;
    
    for(int i = 0 ; i < nombreLigne ; i++){
        for(int j = 0 ; j < nombreColonne ; j++){
            tabBrick[i][j].x = xdebutBrick + j*longueur + (j-1)*5;
            tabBrick[i][j].y = ydebutBrick + i*longueur + (i-1)*5;
            tabBrick[i][j].spawn = 0;
            
            tabBrick[i][j].rebond = 0;
            tabBrick[i][j].temps = 0;

            init_vieBricks(i, j);
        }
    }
    /*---------------------------------------------------------------*/

    nombreJoueur = 2;
}

void drawGame(){
    changeColor(0, 21, 36, 255);
    drawRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    for(int i=0 ; i<nombreJoueur ; i++){
        for(int j=0 ; j<nombreJoueur; j++){
            drawBalle(i, j);
            drawBarre(i);
        }
    }

    for(int i = 0 ; i < nombreLigne ; i++){
        for(int j = 0 ; j < nombreColonne ; j++){
            for(int k=0 ; k<nombreJoueur ; k++){
                drawBricks(i,j,k);
            }
        }
    }

    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
}

void KeyPressed(SDL_Keycode touche){
    switch (touche) {
        case SDLK_q:
            barres[0].vitesse = -VITESSEBARRE;
            break;
        case SDLK_d:
            barres[0].vitesse = VITESSEBARRE;
            break;
        case SDLK_k:
            barres[1].vitesse = -VITESSEBARRE;
            break;
        case SDLK_m:
            barres[1].vitesse = VITESSEBARRE;
            break;
        case SDLK_ESCAPE:
            freeAndTerminate();
            break;
        default:
            break;
    }
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONUP:
                    printf("position de la souris x : %d , y : %d\n", event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}