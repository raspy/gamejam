/*---------------------------------Description---------------------------------*/
/*Nom projet: Brick Breaker             Edit: RaspY             Version: 0_1_0
               Multijoueur

Description Jeu : C'est un casse briques avec plusieurs joueurs.

Description : Apparitions des briques + balles + barres
            Rebond des balles sur les bords de l'écran + barres + briques
            base des différents modes : limite de temps / limite de points

Objectif :  faire les menus

Atteint :   Zblurp
/*-----------------------------------------------------------------------------*/

#include <time.h>
#include <SDL2/SDL.h>
#include <unistd.h>
#include "function.h"

//1920x1080 pixels
#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080
#define FPS 60

/*-------------------------Déclaration Variable--------------------------*/

int yJauge;
int xJaugeVie;
int pointVie;

int xJaugeProprete;
int pointProprete;

int xJaugeSante;
int pointSante;

int hauteurJauge;


/*-------------------------Initialisation Variable--------------------------*/



void init_game(){  

hauteurJauge = 30;
yJauge = 50;


xJaugeVie = 550;
pointVie = 200;

xJaugeProprete = (xJaugeVie + 200 + 100);
pointProprete = 200;

xJaugeSante = (xJaugeProprete + 200 + 100);
pointSante = 200;


}

void drawJaugeVie(){               
    changeColor(114, 246, 82, 255);
    drawRect(xJaugeVie,yJauge,pointVie, hauteurJauge);  
}

void drawJaugeProprete(){                
    changeColor(114, 246, 82, 255);
    drawRect(xJaugeProprete,yJauge,pointProprete, hauteurJauge);  
}

void drawJaugeSante(){                
    changeColor(114, 246, 82, 255);
    drawRect(xJaugeSante,yJauge,pointSante, hauteurJauge);  
}

void drawJauge(){                
    drawJaugeVie();
    drawJaugeProprete();
    drawJaugeSante();
}




void MouseButton(int mouseX, int mouseY){
    // if((hoverStart == 1) && (pageMenu == 0)){
    //     pageMenu = 1;
    // }
}

void MouseMotion(int mouseX, int mouseY){
    // if((page == 0) && (pageMenu == 0) && (mouseX >= 835) && (mouseX <= 1085) && (mouseY >= 655) && (mouseY <= 715)){ 
    //     hoverStart = 1;
    // }
}

void drawGame(){

    actualize();
    usleep(1000000 / FPS); // 60 images par seconde | 1000000 = 1 seconde
    drawJauge();
}

void KeyPressed(SDL_Keycode touche){
    switch (touche) {
        
        case SDLK_ESCAPE:
            freeAndTerminate();        
            break;
        default:
            break;
    }
}

void gameLoop() {
    int programLaunched = 1;
    while (programLaunched == 1) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    programLaunched = 0;
                    break;
                case SDL_MOUSEBUTTONUP:
                    printf("position de la souris x : %d , y : %d\n", event.motion.x, event.motion.y);
                    break;
                case SDL_KEYDOWN:
                    KeyPressed(event.key.keysym.sym);
                    break;
                case SDL_JOYBUTTONDOWN:
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    /* clique de la souris
                     * event.motion.y | event.motion.x pour les positions de la souris
                     */
                    MouseButton(event.motion.x, event.motion.y);
                    break;
                case SDL_MOUSEMOTION:
                    MouseMotion(event.motion.x, event.motion.y);
                    break;
                default:
                    break;
            }
        }
        drawGame();
    }
}

int main(){
    init(WINDOW_WIDTH, WINDOW_HEIGHT);
    init_game();
    gameLoop();
    printf("Fin du programme\n");
    freeAndTerminate();
}